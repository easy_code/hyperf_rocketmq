<?php
/**
 * @Author: laoweizhen <1149243551@qq.com>,
 * @Date: 2022/5/13 23:19,
 * @LastEditTime: 2022/5/13 23:19
 */
declare(strict_types=1);

namespace Lwz\HyperfRocketMQ\Message;

use Hyperf\Database\Model\Model;
use Hyperf\Utils\ApplicationContext;
use Hyperf\Utils\Codec\Json;
use Lwz\HyperfRocketMQ\Constants\MqConstant;
use Lwz\HyperfRocketMQ\Exception\RocketMQException;
use Lwz\HyperfRocketMQ\Model\MqProduceStatusLog;
use Lwz\HyperfRocketMQ\Packer\Packer;

class ProducerMessage extends Message implements ProducerMessageInterface
{
    protected string $messageKey = '';

    protected string $messageTag = '';

    protected string|array $payload = '';

    protected bool $hasSaveStatusLog = false;

    /**
     * 是否记录生产日志.
     */
    protected bool $saveProduceLog = true;

    /**
     * 投递时间（10位时间戳）.
     */
    protected ?int $deliverTime = null;

    public function getMessageKey(): string
    {
        if (! $this->messageKey) {
            $this->setMessageKey(session_create_id('rocketmq'));
        }
        return $this->messageKey;
    }

    public function setMessageKey(string $messageKey): self
    {
        $this->messageKey = $messageKey;
        return $this;
    }

    public function getMessageTag(): string
    {
        return $this->messageTag;
    }

    public function setMessageTag(string $messageTag): self
    {
        $this->messageTag = $messageTag;
        return $this;
    }

    public function setPayload($data): self
    {
        $this->payload = $data;
        return $this;
    }

    public function payload(): string
    {
        return $this->serialize();
    }

    public function getSaveProduceLog(): bool
    {
        return $this->saveProduceLog;
    }

    public function setSaveProduceLog(bool $isSaveLog): self
    {
        $this->saveProduceLog = $isSaveLog;
        return $this;
    }

    public function saveMessageStatus()
    {
        if (! $this->payload()) {
            throw new RocketMQException('请设置payload');
        }

        if ($this->hasSaveStatusLog) {
            return;
        }

        $this->getStatusLogModel()->insert([
            'status' => MqConstant::PRODUCE_STATUS_WAIT,
            'message_key' => $this->getMessageKey(),
            'mq_info' => Json::encode($this->getProduceInfo()),
        ]);
        $this->hasSaveStatusLog = true;
    }

    public function updateMessageStatus(int $status)
    {
        // 情况一：没有开启日志，没有记录状态信息，直接返回
        if ($this->saveProduceLog === false && $this->hasSaveStatusLog === false) {
            return;
        }
        // 情况二：保存了状态信息
        if ($this->hasSaveStatusLog === true) {
            $this->hasSaveStatusLog && $this->getStatusLogModel()
                ->where('message_key', $this->getMessageKey())
                ->update(['status' => $status]);
        } elseif ($this->saveProduceLog) {
            // 情況三：开启了日志，没有保存消息状态
            $this->getStatusLogModel()->insert([
                'status' => $status,
                'message_key' => $this->getMessageKey(),
                'mq_info' => Json::encode($this->getProduceInfo()),
            ]);
        }
    }

    /**
     * 获取生成的消息信息.
     * @param ProducerMessageInterface $producerMessage
     */
    public function getProduceInfo(): array
    {
        return [
            'pool' => $this->getPoolName(),
            'topic' => $this->getTopic(),
            'message_key' => $this->getMessageKey(),
            'message_tag' => $this->getMessageTag(),
            'payload' => $this->payload(),
        ];
    }

    /**
     * Serialize the message body to a string.
     */
    public function serialize(): string
    {
        $packer = ApplicationContext::getContainer()->get(Packer::class);
        return $packer->pack($this->payload);
    }

    public function getDeliverTime(): ?int
    {
        return $this->deliverTime ? $this->deliverTime * 1000 : null;
    }

    public function setDeliverTime(int $timestamp): self
    {
        $this->deliverTime = $timestamp;
        return $this;
    }

    protected function getStatusLogModel(): Model
    {
        return (new MqProduceStatusLog())->setConnection($this->getDbConnection());
    }
}

<?php

declare(strict_types=1);

namespace Lwz\HyperfRocketMQ\Library\Exception;

class InvalidArgumentException extends MQException
{
}
